package character;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {

    @Test
    void testCharIsLevelOne_characterLevelShouldBeOne() {
        Character character = new Mage("Mage");

        int expected = 1;
        int actual = character.getLevel();

        assertEquals(expected, actual);
    }

    @Test
    void testCharacterLevelUp_characterLevelsUp_shouldBeLevelTwo() {
        Character character = new Mage("Mage");

        int expected = 2;
        character.levelUp();
        int actual = character.getLevel();

        assertEquals(expected, actual);
    }

    @Test
    void testMageIsCreatedWithCorrectStats_statsShouldBeEqual() {
        Character character = new Mage("Mage");

        Attribute expected = new Attribute(1, 8, 1, 5);
        Attribute actual = character.getBasePrimaryAttributes();

        assertEquals(expected, actual);
    }

    @Test
    void testRangerIsCreatedWithCorrectStats_statsShouldBeEqual() {
        Character character = new Ranger("Ranger");

        Attribute expected = new Attribute(7, 1, 1, 8);
        Attribute actual = character.getBasePrimaryAttributes();

        assertEquals(expected, actual);
    }

    @Test
    void testRogueIsCreatedWithCorrectStats_statsShouldBeEqual() {
        Character character = new Rogue("Rogue");

        Attribute expected = new Attribute(6, 1, 2, 8);
        Attribute actual = character.getBasePrimaryAttributes();

        assertEquals(expected, actual);
    }

    @Test
    void testWarriorIsCreatedWithCorrectStats_statsShouldBeEqual() {
        Character character = new Warrior("Warrior");

        Attribute expected = new Attribute(2, 1, 5, 10);
        Attribute actual = character.getBasePrimaryAttributes();

        assertEquals(expected, actual);
    }

    @Test
    void testMageIsCreatedLevelUpWithCorrectStats_mageLevelUp_shouldLevelUpAllStatsCorrect() {
        Character character = new Mage("Mage");
        character.levelUp();

        Attribute expected = new Attribute(2, 13, 2, 8);
        Attribute actual = character.getBasePrimaryAttributes();

        assertEquals(expected, actual);
    }

    @Test
    void testRangerIsCreatedLevelUpWithCorrectStats_rangerLevelUp_shouldLevelUpAllStatsCorrect() {
        Character character = new Ranger("Ranger");
        character.levelUp();

        Attribute expected = new Attribute(12, 2, 2, 10);
        Attribute actual = character.getBasePrimaryAttributes();

        assertEquals(expected, actual);
    }

    @Test
    void testRogueIsCreatedLevelUpWithCorrectStats_rogueLevelUp_shouldLevelUpAllStatsCorrect() {
        Character character = new Rogue("Rogue");
        character.levelUp();

        Attribute expected = new Attribute(10, 2, 3, 11);
        Attribute actual = character.getBasePrimaryAttributes();

        assertEquals(expected, actual);
    }

    @Test
    void testWarriorIsCreatedLevelUpWithCorrectStats_warriorLevelUp_shouldLevelUpAllStatsCorrect() {
        Character character = new Warrior("Warrior");
        character.levelUp();

        Attribute expected = new Attribute(4, 2, 8, 15);
        Attribute actual = character.getBasePrimaryAttributes();

        assertEquals(expected, actual);
    }

}