package item;

import character.*;
import character.Character;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {

    // Use Warrior for test character
    // Use Axe for test weapon
    // Use plate body armor for test armor
    // Use Bow for wrong weapon type

    @Test
    void testEquipWeaponLevelTooHigh_equipWeaponLevelTwoToCharacterLevelOne_shouldThrowException() {
        Character character = new Warrior("Warrior");
        Weapon weapon = new Weapon("Axe", 2, Slot.WEAPON, 1, 1, WeaponType.AXE);

        assertThrows(InvalidWeaponException.class, () -> {
            character.equipWeapon(weapon);
        });
    }

    @Test
    void testEquipArmorLevelTooHigh_equipArmorLevelTwoToCharacterLevelOne_shouldThrowException() {
        Character character = new Warrior("Warrior");
        Armor armor = new Armor("Plate", 2, Slot.BODY, new Attribute(1, 1, 1, 1), ArmorType.PLATE);

        assertThrows(InvalidArmorException.class, () -> {
            character.equipArmor(armor);
        });
    }

    @Test
    void testEquipWrongWeaponType_equipBowToWarrior_shouldThrowException() {
        Character character = new Warrior("Arnold");
        Weapon weapon = new Weapon("Bow", 1, Slot.WEAPON, 1, 1, WeaponType.BOW);

        assertThrows(InvalidWeaponException.class, () -> {
          character.equipWeapon(weapon);
        });
    }

    @Test
    void testEquipWrongArmorType_equipClothToWarrior_shouldThrowException() {
        Character character = new Warrior("Arnold");
        Armor armor = new Armor("Cloth", 1, Slot.BODY, new Attribute(1, 1, 1, 1), ArmorType.CLOTH);

        assertThrows(InvalidArmorException.class, () -> {
            character.equipArmor(armor);
        });
    }

    @Test
    void testEquipValidWeapon_equipAxeToWarrior_shouldReturnTrue() {
        Character character = new Warrior("Warrior");
        Weapon weapon = new Weapon("Axe", 1, Slot.WEAPON, 1, 1, WeaponType.AXE);

        boolean expected = true;
        boolean actual = false;
        try {
            actual = character.equipWeapon(weapon);
        } catch(Exception e) {
            e.printStackTrace();
        }

        assertEquals(expected, actual);
    }

    @Test
    void testEquipValidArmor_equipBodyPlateToWarrior_shouldReturnTrue() {
        Character character = new Warrior("Warrior");
        Armor armor = new Armor("Plate", 1, Slot.BODY, new Attribute(1, 1, 1, 1), ArmorType.PLATE);

        boolean expected = true;
        boolean actual = false;
        try {
            actual = character.equipArmor(armor);
        } catch(Exception e) {
            e.printStackTrace();
        }

        assertEquals(expected, actual);
    }

    @Test
    void testDPSWithoutWeapon_shouldReturnExpected() {
        Character character = new Warrior("Warrior");

        double expected = 1*(1 + (5 / 100.0));
        double actual = character.getDPS();

        assertEquals(expected, actual);
    }

    @Test
    void testDPSWithValidWeaponEquipped_shouldReturnExpected() {
        Character character = new Warrior("Warrior");
        Weapon weapon = new Weapon("Axe", 1, Slot.WEAPON, 7, 1.1, WeaponType.AXE);
        try {
            character.equipWeapon(weapon);
        } catch (Exception e) {e.printStackTrace();}

        double expected = (7 * 1.1)*(1 + (5 / 100.0));
        double actual = character.getDPS();

        assertEquals(expected, actual);
    }

    @Test
    void testDPSWithValidWeaponAndArmorEquipped_shouldReturnExpected() {
        Character character = new Warrior("Warrior");
        Weapon weapon = new Weapon("Axe", 1, Slot.WEAPON, 7, 1.1, WeaponType.AXE);
        Armor armor = new Armor("Plate", 1, Slot.BODY, new Attribute(0,0,1,2), ArmorType.PLATE);
        try {
            character.equipWeapon(weapon);
        } catch (Exception e) {e.printStackTrace();}
        try{
            character.equipArmor(armor);
        } catch(Exception e) {e.printStackTrace();}

        double expected = (7 * 1.1) * (1 + ((5+1) / 100.0));
        double actual = character.getDPS();

        assertEquals(expected, actual);
    }
}