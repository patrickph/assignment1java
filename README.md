# Assignment 1 - Build a console application in Java

## RPG Characters

The main functionality of this application is to be able to create characters to a "video game", equip items to the character and calculate the characters total stats based on their items and base stats.

## Requirements

The application is built only using "vanilla" Java.
To see if you have Java installed, run the following command in your terminal:
```bash
java --version
```
If you do not have Java installed, please install this from the web.\
I'm sure you can figure out how ;)

## Execution

The application is mainly run using tests. There is no Main method created for this application.
The tests are implemented using JUnit5.
To run the tests, use an IDE (i.e. IntelliJ), right click on one of the test files and simply press "Run 'testName'".
The tests can be found in the tests directory in the root of the project.