package item;

public enum ArmorType {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE
}
