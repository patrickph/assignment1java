package item;

import character.Attribute;
import character.Slot;

public class Armor extends Item {

    Attribute primaryAttribute;
    ArmorType type;

    public Armor(String name, int requiredLevel, Slot slot, Attribute primaryAttribute, ArmorType type) {
        super(name, requiredLevel, slot);
        this.primaryAttribute = primaryAttribute;
        this.type = type;
    }

    public Attribute getAttributes() {
        return primaryAttribute;
    }

    public void setAttributes(Attribute primaryAttribute) {
        this.primaryAttribute = primaryAttribute;
    }

    public ArmorType getArmorType() {
        return type;
    }

    public void setArmorType(ArmorType type) {
        this.type = type;
    }
}
