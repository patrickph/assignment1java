package item;

import character.Slot;

public class Weapon extends Item {

    double damage;
    double attackSpeed;
    WeaponType type;

    public Weapon(String name, int requiredLevel, Slot slot, double damage, double attackSpeed, WeaponType type) {
        super(name, requiredLevel, slot);
        this.damage = damage;
        this.attackSpeed = attackSpeed;
        this.type = type;
    }

    public double getDamage() {
        // A weapons damage is the weapons damage times its attackspeed - aka the DPS
        return damage * attackSpeed;
    }

    public void setDamage(double damage) {
        this.damage = damage;
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }

    public void setAttackSpeed(double attackSpeed) {
        this.attackSpeed = attackSpeed;
    }

    public WeaponType getWeaponType() {
        return type;
    }

    public void setWeaponType(WeaponType type) {
        this.type = type;
    }
}
