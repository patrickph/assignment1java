package item;

public enum WeaponType {
    AXE,
    BOW,
    DAGGER,
    HAMMER,
    STAFF,
    SWORD,
    WAND
}
