package character;

import item.Armor;
import item.ArmorType;
import item.Weapon;
import item.WeaponType;

public class Rogue extends Character {

    Rogue(String name) {
        super(name, 1, new Attribute(6, 1, 2, 8));
        DPS = 1 + 6 / 100.0;
    }

    public void levelUp() {
        super.levelUp(4, 1, 1, 3);
    }

    public boolean equipWeapon(Weapon w) throws InvalidWeaponException {
        if (w.getWeaponType() != WeaponType.DAGGER && w.getWeaponType() != WeaponType.SWORD) {
            throw new InvalidWeaponException(w.getName() + " cannot be equiped to " + this.name);
        }
        // If required level is too high - throw exception
        if (w.getRequiredLevel() > this.level) {
            throw new InvalidWeaponException("The required level for " + w.getName() + " is too high.\n"
                    + "Required: " + w.getRequiredLevel() + " Character level: " + this.level);
        }
        // Equip weapon
        this.slots.put(Slot.WEAPON, w);

        updateDPS();

        return true;
    }

    public boolean equipArmor(Armor a) throws InvalidArmorException {
        // Checks that the armor is correct type
        if (a.getArmorType() != ArmorType.LEATHER && a.getArmorType() != ArmorType.MAIL) {
            throw new InvalidArmorException(a.getName() + " cannot be equiped to " + this.name);
        } else {
            // Equips the armor and updates the DPS with the updated stats
            boolean r = super.equipArmor(a);
            updateDPS();
            return r;
        }
    }

    private void updateDPS() {
        // Updates the characters DPS according to its stats
        Weapon w = (Weapon) slots.get(Slot.WEAPON);
        if (w != null) {
            DPS = w.getDamage() * (1 + totalPrimaryAttributes.getDexterity() / 100.0);
        } else {
            DPS = (1 + totalPrimaryAttributes.getStrength() / 100.0);
        }
    }
}
