package character;

public class Attribute {

    int dexterity;
    int intelligence;
    int strength;
    int vitality;

    public Attribute(int dexterity, int intelligence, int strength, int vitality) {
        this.dexterity = dexterity;
        this.intelligence = intelligence;
        this.strength = strength;
        this.vitality = vitality;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getVitality() {
        return vitality;
    }

    public void setVitality(int vitality) {
        this.vitality = vitality;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Attribute)) return false;
        Attribute attribute = (Attribute) o;
        return dexterity == attribute.dexterity && intelligence == attribute.intelligence && strength == attribute.strength && vitality == attribute.vitality;
    }
}
