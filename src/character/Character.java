package character;

import item.*;

import java.util.HashMap;

public abstract class Character {

    String name;
    int level;
    double DPS = 1;
    Attribute basePrimaryAttributes;
    Attribute totalPrimaryAttributes;

    HashMap<Slot, Item> slots = new HashMap<Slot, Item>();

    Character(String name, int level, Attribute primary) {
        this.name = name;
        this.level = level;
        this.basePrimaryAttributes = primary;
        // Creates a duplicate of the base attributes when created
        this.totalPrimaryAttributes = new Attribute(basePrimaryAttributes.getDexterity(),
                basePrimaryAttributes.getIntelligence(),
                basePrimaryAttributes.getStrength(),
                basePrimaryAttributes.getVitality());
    }

    public abstract void levelUp();

    protected void levelUp(int dex, int intel, int strength, int vit) {
        // Adds the characters additional stats when leveling up
        level++;
        basePrimaryAttributes.setDexterity(basePrimaryAttributes.getDexterity() + dex);
        basePrimaryAttributes.setIntelligence(basePrimaryAttributes.getIntelligence() + intel);
        basePrimaryAttributes.setStrength(basePrimaryAttributes.getStrength() + strength);
        basePrimaryAttributes.setVitality(basePrimaryAttributes.getVitality() + vit);

        totalPrimaryAttributes.setDexterity(totalPrimaryAttributes.getDexterity() + dex);
        totalPrimaryAttributes.setIntelligence(totalPrimaryAttributes.getIntelligence() + intel);
        totalPrimaryAttributes.setStrength(totalPrimaryAttributes.getStrength() + strength);
        totalPrimaryAttributes.setVitality(totalPrimaryAttributes.getVitality() + vit);
    }

    public abstract boolean equipWeapon(Weapon w) throws InvalidWeaponException;

    public boolean equipArmor(Armor a) throws InvalidArmorException {
        // If required level is too high - throw exception
        if (a.getRequiredLevel() > this.level) {
            throw new InvalidArmorException("The required level for " + a.getName() + " is too high.\n"
                    + "Required: " + a.getRequiredLevel() + " Character level: " + this.level);
        }
        // Equip armor
        Slot slot = a.getSlot();
        this.slots.put(slot, a);

        Attribute newValues = new Attribute(0,0,0,0);
        slots.forEach((key, value) -> {
            if (value instanceof Armor) {
                // Update total of added stats from armor
                Attribute v = ((Armor) value).getAttributes();
                newValues.setDexterity(newValues.getDexterity() + v.getDexterity());
                newValues.setIntelligence(newValues.getIntelligence() + v.getIntelligence());
                newValues.setStrength(newValues.getStrength() + v.getStrength());
                newValues.setVitality(newValues.getVitality() + v.getVitality());
            }
        });

        // Update total attributes
        totalPrimaryAttributes.setDexterity(totalPrimaryAttributes.getDexterity() + newValues.getDexterity());
        totalPrimaryAttributes.setIntelligence(totalPrimaryAttributes.getIntelligence() + newValues.getIntelligence());
        totalPrimaryAttributes.setStrength(totalPrimaryAttributes.getStrength() + newValues.getStrength());
        totalPrimaryAttributes.setVitality(totalPrimaryAttributes.getVitality() + newValues.getVitality());

        return true;
    }

    // Used for attacking?
    public double attack() {
        return this.DPS;
    }

    // Prints the character in a nice way
    @Override
    public String toString() {
        return "Character:\n" +
                "Name: '" + name + "\'\n" +
                "Level: " + level + "\n" +
                "DPS: " + DPS + "\n" +
                "Strength: " + totalPrimaryAttributes.getStrength() + "\n" +
                "Dexterity" + totalPrimaryAttributes.getDexterity() + "\n" +
                "Intelligence" + totalPrimaryAttributes.getIntelligence() + "\n" +
                "Vitality" + totalPrimaryAttributes.getVitality();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public double getDPS() {
        return DPS;
    }

    public void setDPS(double DPS) {
        this.DPS = DPS;
    }

    public Attribute getBasePrimaryAttributes() {
        return basePrimaryAttributes;
    }

    public Attribute getTotalPrimaryAttributes() {
        return totalPrimaryAttributes;
    }
}
