package character;

public enum Slot {
    HEAD,
    BODY,
    LEGS,
    WEAPON
}
